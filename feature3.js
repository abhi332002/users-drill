// Q3 Find all users with masters Degree.

const { users } = require("./userDataSet");

//Define Function
function filterMasterDegreeHolders(users) {

    //first check input is object or not and it's length is greater than 0
    if (typeof (users) !== 'object' || Object.keys(users).length === 0) {
        console.log("Invalid user data. Please provide a non-empty object.");
        return {}; // Return an empty object if personData is invalid or empty

    }

    let userWithMasterDegreeArr = [];

    //iterate the user data 
    for (const userKey in users) {

        //check users nationality
        if (users[userKey].qualification== "Masters") {
            userWithMasterDegreeArr.push(userKey);
        }
    }
    
    return userWithMasterDegreeArr;
}

filterMasterDegreeHolders(users);
//export function
module.exports = { filterMasterDegreeHolders };