// Q4 Group users based on their Programming language mentioned in their designation.

const { users } = require("./userDataSet");

//Define function 
function groupUsersBasedOnDesignation(users) {

    //first check input is object or not and it's length is greater than 0
    if (typeof (users) !== 'object' || Object.keys(users).length === 0) {
        console.log("Invalid user data. Please provide a non-empty object.");
        return {}; // Return an empty object if personData is invalid or empty

    }

    //Initialise the objects with empty array
    let userDesgination = { "Python Developers": [], "Golang Developers": [], "Javascript Developers": [] }


    //iterate in users data
    for (const userKey in users) {

        let user = users[userKey];

        if (user.desgination.includes("Golang")) {

            userDesgination["Golang Developers"].push(userKey);

        } else if (user.desgination.includes("Python")) {

            userDesgination["Python Developers"].push(userKey);

        } else if (user.desgination.includes("Javascript")) {

            userDesgination["Javascript Developers"].push(userKey);

        }

    }

    return userDesgination;

}

//export function 
module.exports = { groupUsersBasedOnDesignation };