const { filterMasterDegreeHolders } = require("../feature3");
const { users } = require("../userDataSet");


let usersWithMasterDegree = filterMasterDegreeHolders(users);

if (usersWithMasterDegree) {
    console.log(usersWithMasterDegree);
} else {
    console.log("User data not found !");
}