// Q2 Find all users staying in Germany.

//Define function 
function filterGermanyUsers(users) {

    //first check input is object or not and it's length is greater than 0
    if (typeof (users) !== 'object' || Object.keys(users).length === 0) {
        console.log("Invalid user data. Please provide a non-empty object.");
        return {}; // Return an empty object if personData is invalid or empty

    }

    let germanyUsersArr = [];

    //iterate the user data 
    for (const userKey in users) {

        //check users nationality
        if (users[userKey].nationality == "Germany") {
            germanyUsersArr.push(userKey);
        }
    }
    return germanyUsersArr;
}

// export function
module.exports = { filterGermanyUsers };