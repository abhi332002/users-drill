// Q1 Find all users who are interested in playing video games.

//Define function 
function filterUserLikeVideoGames(users) {

    //first check input is object or not and it's length is greater than 0
    if (typeof (users) !== 'object' || Object.keys(users).length === 0) {
        console.log("Invalid user data. Please provide a non-empty object.");
        return {}; // Return an empty object if personData is invalid or empty

    }

    let userLikeVideoGames = [];

    //iterate an user data 
    for (const key in users) {

        //store interests in array
        let interestsArr = users[key].interests;

        for (let index = 0; index < interestsArr.length; index++) {

            let interestString = "Video Games";

            if (interestsArr[index].includes(interestString)) {
                userLikeVideoGames.push(key);
            }

        }
    }

    //return 
    return userLikeVideoGames;
}


//export function 
module.exports = { filterUserLikeVideoGames };